let buttons = [];
let wrapper = document.querySelector('.btn-wrapper');
wrapper.querySelectorAll('.btn').forEach(el=>{
    let content = el.textContent;
    content = content.toLowerCase(); //если проверять с разными регистрами
    el.dataset.value = content;
    buttons.push(content);
});
window.addEventListener('keyup', changeColorButton);
function changeColorButton(e) {

    let key = e.key;
    key = key.toLowerCase();  //если проверять с разными регистрами
    console.log(key);

    removeClassActive();

    if(buttons.includes(key)){
        let element = wrapper.querySelector(`.btn[data-value=${key}]`);
        addClassActive(element);
    }
    function addClassActive(el) {
       el.classList.add('active');

    }
    function removeClassActive() {
       let active = wrapper.querySelector('.btn.active');
       if(active){
           active.classList.remove('active');
       }
    }
}
